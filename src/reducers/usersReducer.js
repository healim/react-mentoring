export const GET_USERS = "user/get_user"
export const DATA_INITIALIZED = "user/data_initialized"

const initialState = {
  metadata: [],
  isDataInitialized: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case DATA_INITIALIZED: 
      return {
        ...state, 
        metadata: action.metadata,
        isDataInitialized: true
      }
    default:
      return state
  }
}

