import { combineReducers } from "redux"
// combine sub-reducers to root-reducer
import form from './formReducer'
import users from './usersReducer'

export default combineReducers({
  form,
  users
})

