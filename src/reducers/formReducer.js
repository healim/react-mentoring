export const CHANGE_VALUE = "form/change_value"

const products = [
  {
    name: "귀여운 스티커",
    price: 1000,
    stock: 10
  },
  {
    name: "유용한 메모지",
    price: 2000,
    stock: 20
  },
  {
    name: "홀로그램 씰스티커",
    price: 3000,
    stock: 10
  }
]

const marketSampleInfo = {
  title: "1차 통판 엽니다",
  description: "귀여운 스티커와 문구 친구들 통판 진행 합니다.",
  products
}

const initialState = {
  market: marketSampleInfo
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_VALUE:
      return {
        ...state,
        market: action.market
      };
  
    default:
      return state;
  }
};