import React from 'react'

export default ({marketInfo, handleChange}) => {
  const {title, description } = marketInfo
  return (
    <>
      <h1>마켓 등록하기</h1>
      <form>
        <input
          type="text"
          name="title"
          placeholder="마켓 제목 입력"
          defaultValue={title}
          onChange={handleChange}
        />
        <input
          type="text"
          name="description"
          placeholder="마켓 설명 입력"
          defaultValue={description}
          onChange={handleChange}
        />
      </form>
    </>
  );
}

// <input type={type} name={name} placeholder={placeholder} defaultValue={defaultValue} />
