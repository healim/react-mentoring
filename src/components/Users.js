import React from 'react'
import { User } from './index'

export default function Users({users, usersFromRedux}) {
 
  return (
    <>
      {console.log(
        `A. users
          - typeof: ${typeof users}
          - Array.isArray: ${Array.isArray(users)}
          - data: ${JSON.stringify(users)}`
      )}
      {console.log(
        `B. usersFromRedux
          - typeof: ${typeof usersFromRedux}
          - Array.isArray: ${Array.isArray(usersFromRedux)}
          - data: ${JSON.stringify(usersFromRedux)}`
      )}
      <h1>users</h1>
      {JSON.stringify(users)}}<h1>using map</h1>
      <User user={users[0]} />
      {users.map(user => {
        return <User key={user.id} user={user} />;
      })}
      <h1>users from redux</h1>
      <User user={users[0]} />
      {JSON.stringify(usersFromRedux)}}<h1>using map</h1>
      {
        usersFromRedux.map(user => {
          return <User key={user.id} user={user} />;
        })
      }
    </>
  );
}

// 여기서는 defaultProps 써도 소용 없음
// Users.defaultProps = {
//   users: [{ name: "users is undefined" }]
// };
