import React, { useEffect, useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { DATA_INITIALIZED } from '../reducers/usersReducer'
import { getUsers } from '../utils/callApi'

export default function useUsers(){
  const [users, setUsers] = useState([])
  // const [users, setUsers] = useState(useSelector(state => state.users.metadata));
  const [isDataInitialized, setIsDataInitialized] = useState(useSelector(state => state.users.isDataInitialized))
  const dispatch = useDispatch()

  const initialize = useCallback(async () => {
    if (isDataInitialized === false) {
      let metadata = await getUsers();
      await setIsDataInitialized(true);
      await dispatch({
        type: DATA_INITIALIZED,
        metadata,
        isDataInitialized: true
      });
      setUsers(metadata);
    }
  }, [dispatch, isDataInitialized]);

  useEffect(() => {
    initialize()
  }, [initialize])

  return [users, isDataInitialized]
}