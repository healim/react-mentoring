import React from 'react'
import { useDispatch, useSelector } from "react-redux";
import { CHANGE_VALUE } from '../reducers/formReducer'

export default function useForm(){
  const dispatch = useDispatch()
  const form = useSelector(state => state.form);
  const { market : marketInfo } = form

  const handleChange = e => {
    e.persist()
    dispatch({ type: CHANGE_VALUE, market: {
      ...marketInfo,
      [e.target.name]: e.target.value
    } });
  }

  return { marketInfo, handleChange }
}