import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  Home,
  Login,
  Seller,
  CreateMarket,
  Users,
  NotFound
} from "./pages"

export default function App() {
  return (
    <Router>
      <nav>
        <Link to="/home">메인 페이지 이동</Link>
      </nav>
      <Switch>
        <Route exact path="/"><Users /></Route>
        <Route path="/home"><Home /></Route>
        <Route path="/login"><Login /></Route>
        <Route path="/seller"><Seller /></Route>
        <Route path="/create-market"><CreateMarket /></Route>
        <Route path="*"><NotFound /></Route>
      </Switch>
    </Router>
  );
}

