import React from 'react'
import useUsers from '../hooks/useUsers'
import Users from '../components/Users'
import { useSelector } from "react-redux";

export default function UsersInterface() {
  const [users, usersFromRedux, isDataInitialized] = useUsers()
  const isDataInitializedFromRedux = useSelector(state => state.users.isDataInitialized)

  return (
    <>
      {isDataInitialized ? (
        <Users users={users} usersFromRedux={usersFromRedux} />
      ) : (
        "로딩 중..."
      )}
    </>
  );
}