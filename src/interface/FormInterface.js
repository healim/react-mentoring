import React from 'react'
import { Form } from '../components' 
import useForm from '../hooks/useForm'

export default function FormInterface() {
  const { marketInfo, handleChange } = useForm()
  
  return (
    <Form
      marketInfo={marketInfo}
      handleChange={handleChange}
    />
  );  
}