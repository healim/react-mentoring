import React from 'react'
import { FormInterface } from '../interface'


export default () => {
  return (
    <div>
      <h1>주문서 만드는 페이지</h1>
      <FormInterface />
    </div>
  )
}
