import axios from 'axios'

// using json placeholder for example
const BASE_URL =
  "https://jsonplaceholder.typicode.com";
const resources = {
  user : "/users"
}

export async function getUsers() {
  try {
    const res = await axios.get(BASE_URL + resources.user);
    return res.data
  } 
  catch (error) {
    return error
  }
}

