import "dotenv/config";
import cors from "cors";
import path from "path";
import express from "express";

const app = express();
const PORT = process.env.PORT || 8887;

app.use(cors())
app.use(express.static(path.join(__dirname, "build")));
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(PORT, () => {
  console.log(`listening on \nhttp://localhost:${PORT}`);
});
